# Changelog

## 1.0.0 (2019-09-13)

* First tagged release

## 1.0.1 (2020-03-17)

* Db log target support for logged job
  Migrate your database
  `php yii migrate --migrationPath=@yii/log/migrations/ --migrationTable='scheme.migration'`

* add config examples
* add example job with db log target
* 4 channels
* module support separate redis config for queue

## 2.0.0 (2022-09-12)

* Refactoring
* Last Yii2 version support