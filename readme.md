<h1>Multi channels worker module for yii2</h1>

<h2>Install</h2>

```bash
composer require raid3r81/worker-module
```

<h2>Config</h2>

<h3>Simple usage</h3>

Add to modules section of config:

```php
        'worker' => [
            'class' => \Rf\Modules\Worker\SimpleModule::class,
        ],
```

<h3>Advanced usage</h3>

Add to modules section of config:

2 channels (queues) 

```php
'worker' => [
            'class' => \Rf\Modules\Worker\Module::class,
            'aliases' => [
                '@worker_channel1' => '@runtime/worker/channel1',
                '@worker_channel2' => '@runtime/worker/channel2'
            ],
            'components' => [
                'channel1' => [
                    'class' => \yii\queue\file\Queue::class,
                    'path' => '@worker_channel1',
                    'as log' => \yii\queue\LogBehavior::class,
                ],
                'channel2' => [
                    'class' => \yii\queue\file\Queue::class,
                    'path' => '@worker_channel2',
                    'as log' => \yii\queue\LogBehavior::class,
                ],
            ],
            'params' => [],
        ],
```

4 channels (queues) 

```php
'worker' => [
            'class' => \Rf\Modules\Worker\Module::class,
            'aliases' => [
                '@worker_channel1' => '@runtime/worker/channel1',
                '@worker_channel2' => '@runtime/worker/channel2',
                '@worker_channel3' => '@runtime/worker/channel3',
                '@worker_channel4' => '@runtime/worker/channel4'
            ],
            'components' => [
                'channel1' => [
                    'class' => \yii\queue\file\Queue::class,
                    'path' => '@worker_channel1',
                    'as log' => \yii\queue\LogBehavior::class,
                ],
                'channel2' => [
                    'class' => \yii\queue\file\Queue::class,
                    'path' => '@worker_channel2',
                    'as log' => \yii\queue\LogBehavior::class,
                ],
                'channel3' => [
                    'class' => \yii\queue\file\Queue::class,
                    'path' => '@worker_channel3',
                    'as log' => \yii\queue\LogBehavior::class,
                ],
                'channel4' => [
                    'class' => \yii\queue\file\Queue::class,
                    'path' => '@worker_channel4',
                    'as log' => \yii\queue\LogBehavior::class,
                ],
            ],
            'params' => [],
        ],
```

<h2>Use</h2>

<h3>Console commands</h3>

Run by channel name

```bash
#run worker
php yii worker/channel1/run
php yii worker/channel2/run
php yii worker/channel3/run
php yii worker/channel4/run

```

Listen by channel name

```bash
#run worker
php yii worker/channel1/listen
php yii worker/channel2/listen
php yii worker/channel3/listen
php yii worker/channel4/listen

```

<h3>Code examples</h3>

```php

        /**
         * @var WorkerInterface $worker
         */
        $worker = \Yii::$app->getModule('worker');

        $job = new ExampleJob([
            'logToConsole' => true
        ]);
        
        //push job to channel1
        $worker->addJob($job, 'channel1');
        
        //push job and get job id
        $id = $worker->addJob($job, 'channel1')->getLastAddedJobId('channel1');
        
        //push job with start delay 10 seconds to channel1 
        $worker->delay(10)->addJob($job, 'channel1');
        
        //push job and run async process with worker (immediately run) 
        $worker->execute($job, 'channel1');
        
        //push three jobs and run async 2 process with workers for processing
        $worker->addJob($job, 'channel1');
        $worker->addJob($job, 'channel1');
        $worker->addJob($job, 'channel1');
        $worker->run(2, 'channel1');

```

<h2>Contacts</h2>

Vasyl Kovalov | Василий Ковалёв  
[raid3r81@gmail.com](mailto:raid3r81@gmail.com)  
skype: [raid3r81](skype:<raid3r81>?call)