<?php

//Simple module config

$params = require __DIR__ . '/params.php';

$config = [
    // config
    'params' => $params,
    'aliases' => [
        '@worker_channel1' => '@runtime/worker/channel1',
        '@worker_channel2' => '@runtime/worker/channel2',
        '@worker_channel3' => '@runtime/worker/channel3',
        '@worker_channel4' => '@runtime/worker/channel4'
    ],
    'components' => [
        'channel1' => [
            'class' => \yii\queue\file\Queue::class,
            'path' => '@worker_channel1',
            'as log' => \yii\queue\LogBehavior::class,
        ],
        'channel2' => [
            'class' => \yii\queue\file\Queue::class,
            'path' => '@worker_channel2',
            'as log' => \yii\queue\LogBehavior::class,
        ],
        'channel3' => [
            'class' => \yii\queue\file\Queue::class,
            'path' => '@worker_channel3',
            'as log' => \yii\queue\LogBehavior::class,
        ],
        'channel4' => [
            'class' => \yii\queue\file\Queue::class,
            'path' => '@worker_channel4',
            'as log' => \yii\queue\LogBehavior::class,
        ],
    ],

];

return $config;