<?php
/**
 * Developed by
 * @author Vasyl Kovalov <raid3r81@gmail.com>
 * Date: 20.08.2019
 *
 */

namespace Rf\Modules\Worker;

use yii\queue\JobInterface;
use yii\queue\Queue;

/**
 * @package Rf\Modules\Worker
 *
 * @version 2.0.0
 * @author Vasyl Kovalov raid3r81@gmail.com
 *
 * @property  Queue $channel1
 * @property  Queue $channel2
 */
interface WorkerInterface
{

    /**
     * @param string $channel
     *
     * @return null|integer
     */
    public function getLastAddedJobId(string $channel): ?int;

    /**
     * @param JobInterface $job
     * @param string       $channel
     *
     * @return Module
     */
    public function execute(JobInterface $job, string $channel): WorkerInterface;

    /**
     * @param int    $count
     * @param string $channel
     *
     * @return $this
     */
    public function run(int $count, string $channel): WorkerInterface;

    /**
     * @param JobInterface $job
     * @param string       $channel
     *
     * @return $this
     */
    public function addJob(JobInterface $job, string $channel): WorkerInterface;

    /**
     * @param int $delay
     *
     * @return WorkerInterface
     */
    public function delay(int $delay): WorkerInterface;
}