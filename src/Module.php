<?php

namespace Rf\Modules\Worker;

use ptlis\ShellCommand\CommandBuilder;
use Yii;
use yii\console\Application;
use yii\log\FileTarget;
use yii\queue\JobInterface;

/**
 * Class Module
 *
 * @package Rf\Modules\Worker
 *
 * @property $workerRedis
 */
class Module extends \yii\base\Module implements WorkerInterface
{
    use OwnLogTrait;

    const CHANNEL1 = 'channel1';
    const CHANNEL2 = 'channel2';
    const CHANNEL3 = 'channel3';
    const CHANNEL4 = 'channel4';

    const LOG_CATEGORY = 'worker';
    /**
     * @var array
     */
    protected array $lastAddedJobId
        = [
        ];
    protected $_delay;

    public function init()
    {
        Yii::$app->setAliases([
                                  '@Rf/Modules/Worker' => __DIR__,
                              ]);

        $this->registerLogTarget(new FileTarget([
            'levels' => $this->logLevels,
            'categories' => [self::LOG_CATEGORY],
            'logVars' => [],
            'logFile' => '@runtime/logs/' . self::LOG_CATEGORY . '.log',
            'enabled' => $this->logEnabled
        ]), self::LOG_CATEGORY);

        if (isset($this->components['workerRedis'])) {
            Yii::$container->setDefinitions([
                                                'workerRedis' => $this->workerRedis,
                                            ]);
        }

        $this->setConsoleControllerNamespace();


    }

    protected function setConsoleControllerNamespace()
    {
        if (Yii::$app instanceof Application) {
            $this->controllerNamespace = __NAMESPACE__ . '\commands';
        }

        //register queue console command
        if (Yii::$app instanceof Application) {
            foreach ($this->components as $name => $channelConfig) {
                if ($name == 'workerRedis') {
                    continue;
                }

                $this->controllerMap[$name] = [
                    'class' => $this->$name->commandClass,
                    'queue' => $this->$name,
                ];
            }

        }
    }

    /**
     * @return null|integer
     */
    public function getLastAddedJobId($channel = self::CHANNEL1): ?int
    {
        return $this->lastAddedJobId[$channel] ?? null;
    }

    /**
     * @param JobInterface $job
     * @return Module
     */
    public function execute(JobInterface $job, $channel = self::CHANNEL1): WorkerInterface
    {
        Yii::debug('Execute job at ' . $channel);
        return $this->addJob($job, $channel)->run(1);
    }

    /**
     * @param JobInterface $job
     * @return $this
     */
    public function addJob(JobInterface $job, $channel = self::CHANNEL1): WorkerInterface
    {
        if ($this->_delay) {
            $this->$channel->delay($this->_delay);
            $this->_delay = null;
        }

        $id = $this->$channel->push($job);
        $this->lastAddedJobId[$channel] = $id;

        return $this;
    }

    /**
     * @param int $count
     * @return $this
     */
    public function run(int $count = 1, $channel = self::CHANNEL1): WorkerInterface
    {
        foreach (range(1, $count) as $workerCount) {
            $this->startWorker($channel);
        }
        return $this;
    }

    protected function startWorker($channel = self::CHANNEL1): bool
    {
        $builder = new CommandBuilder();

        $path = Yii::getAlias('@app') . DIRECTORY_SEPARATOR;

        $command = $builder->setCommand('php')
                           ->addArgument($path . 'yii')
                           ->addArgument("worker/{$channel}/run")
                           ->buildCommand();

        $r = $command->runAsynchronous();

        return $r->isRunning();
    }

    /**
     * @param int $delay
     * @return WorkerInterface
     */
    public function delay(int $delay): WorkerInterface
    {
        $this->_delay = $delay;
        return $this;
    }
}