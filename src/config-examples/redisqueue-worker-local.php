<?php

return [
    'class' => \Rf\Modules\Worker\Module::class,
    'aliases' => [
        '@worker_channel1' => '@runtime/worker/channel1',
        '@worker_channel2' => '@runtime/worker/channel2',
        '@worker_channel3' => '@runtime/worker/channel3',
        '@worker_channel4' => '@runtime/worker/channel4'
    ],
    'components' => [
        'workerRedis' => [
            'class' => \yii\redis\Connection::class,
            'hostname' => '127.0.0.1',
            'port' => 6379,
            'database' => 4, // Make unique for worker instance!
        ],
        'channel1' => [
            'class' => \yii\queue\redis\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'redis' => 'workerRedis', // Компонент подключения к Redis или его конфиг
            'channel' => \Rf\Modules\Worker\Module::CHANNEL1, // Ключ канала очереди
        ],
        'channel2' => [
            'class' => \yii\queue\redis\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'redis' => 'workerRedis', // Компонент подключения к Redis или его конфиг
            'channel' => \Rf\Modules\Worker\Module::CHANNEL2, // Ключ канала очереди
        ],
        'channel3' => [
            'class' => \yii\queue\redis\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'redis' => 'workerRedis', // Компонент подключения к Redis или его конфиг
            'channel' => \Rf\Modules\Worker\Module::CHANNEL3, // Ключ канала очереди
        ],
        'channel4' => [
            'class' => \yii\queue\redis\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'redis' => 'workerRedis', // Компонент подключения к Redis или его конфиг
            'channel' => \Rf\Modules\Worker\Module::CHANNEL4, // Ключ канала очереди
        ],
    ]
];
