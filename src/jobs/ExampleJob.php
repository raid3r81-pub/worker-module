<?php

namespace Rf\Modules\Worker\jobs;

class ExampleJob extends LoggedJob
{

    public function execute($queue)
    {
        $this->initServices(); // REQUIRED!

        $this->info('Start: ' . $this->getUniqueName());

        $this->info('Log info message');
        $this->debug('Log debug message');
        $this->warning('Log warning message');
        $this->error('Log error message');

        $this->info('End: ' . $this->getUniqueName());
    }

    public function getUniqueName(): string
    {
        return 'example';
    }
}

