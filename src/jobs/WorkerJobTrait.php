<?php

namespace Rf\Modules\Worker\jobs;

use Rf\Modules\Worker\WorkerInterface;
use Yii;
use yii\queue\JobInterface;

trait WorkerJobTrait
{

    /**
     * @var WorkerInterface
     */
    public $worker;
    /**
     * @var string Канал воркера
     * @see Module
     */
    public string $workerChannel = 'channel1';

    /**
     * @param JobInterface $job
     */
    protected function pushJob(JobInterface $job)
    {
        $channel = $this->workerChannel;

        return $this->worker->addJob($job, $channel)
                            ->getLastAddedJobId($channel);
    }

    protected function initWorker(): void
    {
        if ( ! $this->worker) {
            $this->worker = Yii::$app->getModule('worker');
        }
    }
}
