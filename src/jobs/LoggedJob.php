<?php

namespace Rf\Modules\Worker\jobs;

use Rf\Modules\Worker\OwnLogTrait;
use yii\base\Model;
use yii\log\Target;
use yii\queue\JobInterface;

/**
 * Class LoggedJob
 *
 * @package Rf\Modules\Worker\jobs
 */
abstract class LoggedJob extends Model implements JobInterface
{

    use OwnLogTrait;

    abstract public function getUniqueName(): string;

    protected function initServices(): void
    {
        $this->registerLogTarget($this->getLogTarget(), $this->getUniqueName());
    }

    /**
     * @return Target
     */
    protected function getLogTarget(): Target
    {
        return $this->getFileLogTarget($this->getUniqueName());
    }
}

