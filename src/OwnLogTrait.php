<?php

namespace Rf\Modules\Worker;

use Yii;
use yii\helpers\BaseConsole;
use yii\helpers\Console;
use yii\log\DbTarget;
use yii\log\FileTarget;
use yii\log\Logger;
use yii\log\Target;

trait OwnLogTrait
{

    /**
     * @var array
     */
    public array $logLevels = ['trace', 'info', 'warning', 'error'];
    /**
     * @var bool
     */
    public bool $logEnabled = true;
    /**
     * @var bool
     */
    public bool $logToConsole = false;

    /**
     * @param Target $target
     * @param        $category
     */
    protected function registerLogTarget(Target $target, $category)
    {
        $target->categories = [$category];
        $target->levels     = $this->logLevels;

        Yii::$app->log->targets[$category] = $target;
    }

    /**
     * @param $message
     */
    protected function debug($message)
    {
        if ($this->logToConsole && $this->logEnabled) {
            Console::output($message);
        }
        //Do not use \Yii::debug() - depends from YII_DEBUG defined
        Yii::getLogger()->log($message, Logger::LEVEL_TRACE, $this->getUniqueName());
    }

    /**
     * @param $message
     */
    protected function error($message)
    {
        if ($this->logToConsole && $this->logEnabled) {
            Console::output(Console::ansiFormat($message, [BaseConsole::FG_RED]));
        }
        Yii::error($message, $this->getUniqueName());
    }

    /**
     * @param $message
     */
    protected function warning($message)
    {
        if ($this->logToConsole && $this->logEnabled) {
            Console::output(Console::ansiFormat($message, [BaseConsole::FG_YELLOW]));
        }
        Yii::warning($message, $this->getUniqueName());
    }

    /**
     * @param $message
     */
    protected function info($message)
    {
        if ($this->logToConsole && $this->logEnabled) {
            Console::output(Console::ansiFormat($message, [BaseConsole::FG_BLUE]));
        }
        Yii::info($message, $this->getUniqueName());
    }

    /**
     * @param $message
     */
    protected function success($message)
    {
        if ($this->logToConsole && $this->logEnabled) {
            Console::output(Console::ansiFormat($message, [BaseConsole::FG_GREEN]));
        }
        Yii::info($message, $this->getUniqueName());
    }

    /**
     * @return Target
     * @throws \yii\base\InvalidConfigException
     */
    protected function getFileLogTarget($filename)
    {
        Yii::$app->log->flushInterval = 1;

        return Yii::createObject(
            [
                'class'          => FileTarget::class,
                'logVars'        => [],
                'logFile'        => '@runtime/logs/' . date("Ymd") . DIRECTORY_SEPARATOR
                                    . $filename . '.log',
                'prefix'         => function ($message) {
                    return "";
                },
                'enabled'        => $this->logEnabled,
                'exportInterval' => 1,
            ]
        );
    }

    /**
     * @return Target
     * @throws \yii\base\InvalidConfigException
     */
    protected function getDbLogTarget($db = 'db', $logTable = '{{%log}}')
    {
        return Yii::createObject(
            [
                'class'    => DbTarget::class,
                'db'       => $db,
                'logTable' => $logTable,
                'logVars'  => [],
                'prefix'   => function ($message) {
                    return "";
                },
                'enabled'  => $this->logEnabled,
            ]
        );
    }
}