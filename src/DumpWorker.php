<?php

namespace Rf\Modules\Worker;

use yii\base\Model;
use yii\helpers\Console;
use yii\helpers\VarDumper;
use yii\queue\JobInterface;

class DumpWorker extends Model implements WorkerInterface
{
    const CHANNEL1 = 'channel1';
    const CHANNEL2 = 'channel2';

    public array $jobs
        = [
            self::CHANNEL1 => [],
            self::CHANNEL2 => [],
        ];

    protected $lastAddedJobId = [
        self::CHANNEL1 => null,
        self::CHANNEL2 => null,
    ];

    /**
     * @return null|integer
     */
    public function getLastAddedJobId($channel = self::CHANNEL1): ?int
    {
        return $this->lastAddedJobId[$channel];
    }

    /**
     * @param JobInterface $job
     * @return Module
     */
    public function execute(JobInterface $job, $channel = self::CHANNEL1): WorkerInterface
    {
        return $this->addJob($job, $channel)->run(1);
    }

    /**
     * @param JobInterface $job
     * @param string       $channel
     *
     * @return $this
     */
    public function addJob(JobInterface $job, string $channel = self::CHANNEL1): WorkerInterface
    {
        $this->lastAddedJobId[$channel] = null;
        $this->lastAddedJobId[$channel] = count($this->jobs[$channel]) + 1;
        $this->jobs[$channel][]         = $job;

        Console::output("Add job to channel: " . $channel);
        Console::output('Job: ' . get_class($job));
        Console::output('Data: ' . VarDumper::dumpAsString($job));

        return $this;
    }

    /**
     * @param int    $count
     * @param string $channel
     *
     * @return $this
     */
    public function run(int $count = 1, string $channel = self::CHANNEL1): WorkerInterface
    {
        foreach (range(1, $count) as $workerCount) {
            Console::output("Start worker for " . $channel);
        }

        return $this;
    }

    public function delay(int $delay): WorkerInterface
    {
        Console::output("Delay: " . $delay);
        return $this;
    }
}