<?php

namespace Rf\Modules\Worker;

class SimpleModule extends Module
{
    public function init()
    {
        \Yii::configure($this, require __DIR__ . DIRECTORY_SEPARATOR .
            'config' . DIRECTORY_SEPARATOR .
            'config.php');

        parent::init();
    }
}